Custom Stage for OBS
====================

This is a [OpenLP custom stage view](https://manual.openlp.org/stage_view.html)
which shows song and custom slides texts in the lower third of an OBS scene.

Installation
------------

1. Unpack the contents of this repo into $OPENLP/stages/obs where OPENLP is the
   OpenLP data directory.

2. Create a browser view in OBS which shows http://$HOST:4316/stage/obs. HOST is
   the host name or IP address of the computer runnning OpenLP.

3. Add browser view to relevant scenes.

Only text in song and custom slides gets displayed - no bible verses or media
titles. The display is aligned to the lower edge. Use a 2 lines design for best
readability.

This code is forked and adapted from
https://github.com/amirchev/OBS-OpenLP-Lyrics-Interface/.
