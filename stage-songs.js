var fadeinout = 250;

window.OpenLP = { // Connect to the OpenLP Remote WebSocket to get pushed updates
    myWebSocket: function (data, status) {
        const host = window.location.hostname;
        const websocket_port = 4317;
        var curmode;

        ws = new WebSocket(`ws://${host}:${websocket_port}`);
        ws.onmessage = (event) => {
            const reader = new FileReader();
            reader.onload = () => {
                data = JSON.parse(reader.result.toString()).results;
                OpenLP.curStatus = (data.theme || data.display || data.blank) ? 'hide' : 'live';

                if (OpenLP.currentItem != data.item ||
                    OpenLP.currentService != data.service) {
                    //New item has been selected
                    OpenLP.currentItem = data.item;
                    OpenLP.currentService = data.service;
                    OpenLP.loadSlides();
                }
                else if (OpenLP.currentSlide != data.slide) {
                    // New Slide has been selected
                    OpenLP.currentSlide = parseInt(data.slide, 10);
                    OpenLP.updateSlide();
                } else {
                    //catch all just reload
                    OpenLP.loadService();
                }
            };
            reader.readAsText(event.data);
        };
    },

    loadService: function (event) {
        $.getJSON(
            "/api/v2/service/items",
            function (data, status) {
                OpenLP.nextSong = "";
                data.forEach(function(item, index, array) {
                    if (item.selected) {
                        OpenLP.currentItem = item;
                        OpenLP.curPlugin =data[index].plugin;
                        OpenLP.updateSlide();
                    }
                });
            }
        );
    },

    loadSlides: function (event) {
        $.getJSON(
            "/api/v2/controller/live-items",
            function (data, status) {
                OpenLP.currentSlides = data.slides;
                OpenLP.currentSlide = 0;
                $.each(data.slides, function(idx, slide) {
                    if (slide["selected"])
                        OpenLP.currentSlide = idx;
                })
                OpenLP.loadService();
            }
        );
    },

    updateSlide: function() {
        var slide = OpenLP.currentSlides[OpenLP.currentSlide];
        var text = "";

        // use title as alternative
        if (slide["text"]) {
            text = slide["text"];
        } else {
            text = slide["title"];
        }
        text = text.replace(/\n/g, "<br />");

        const plugin = OpenLP.curPlugin;
        if (!(plugin == "songs" || plugin == "custom") || OpenLP.curStatus != 'live') {
            //hide the text if not songs
            text = "";
        }

        if (OpenLP.curtext != text) {
            var fade = (OpenLP.curtext == "" || text == "") ? fadeinout : 0;
            OpenLP.curtext = text
            $("#currentslide").fadeOut(fade, function() {
                $("#currentslide").html(OpenLP.curtext);
            });
            $("#currentslide").fadeIn(fade);
        }
    },
}

$.ajaxSetup({ cache: false });
OpenLP.myWebSocket();
